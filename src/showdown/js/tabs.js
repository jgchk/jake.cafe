import $ from 'jquery'

export default class Tabs {
    constructor() {
        this.tabs = {}
        this.current_tab_id = null
    }

    get current_tab() {
        return this.tabs[this.current_tab_id] || null
    }

    set current_tab(tab) {
        if (typeof tab === 'string') tab = this.tabs[tab]
        this.current_tab_id = tab.id
        tab.activate()
    }

    add_tab(tab, activate = true) {
        this.tabs[tab.id] = tab
        if (activate) this.current_tab = tab
    }

    remove_tab(tab) {
        tab.close()
        if (this.current_tab === tab) {
            this.current_tab = $('#tabs')
                .children()
                .last()
                .attr('id')
        }
        delete this.tabs[tab.id]
    }
}
