import Dexie from 'dexie'

const db = new Dexie('Showdown')
db.version(1).stores({
    replays: '&id, format',
})

export default {
    add(id) {
        const format = id.split('-')[0]
        return db.replays.get(id).then(battle => {
            if (battle === undefined)
                return fetch(`/showdown/replay/${id}`)
                    .then(response => response.text())
                    .then(battle_log => {
                        battle = { id: id, format: format, log: battle_log }
                        return db.replays
                            .add(battle)
                            .then(() => Promise.resolve(battle))
                    })
            else return Promise.resolve(false)
        })
    },

    all(format) {
        if (format) return db.replays.where({ format: format }).toArray()
        else return db.replays.toArray()
    },
}
