import EventDispatcher from '../../common/js/event_dispatcher.js'
import SockJS from 'sockjs-client'

export default class Socket extends EventDispatcher {
    constructor() {
        super()
        this.connection = new SockJS('https://sim2.psim.us:443/showdown')
        this.connection.onmessage = e => this.handle_message(e.data)
    }

    send(message, room_id = '') {
        if (room_id === 'global') room_id = ''
        const sent_message = [room_id, message].join('|')
        console.log(`>> ${sent_message}`)
        this.connection.send(sent_message)
    }

    handle_message(message) {
        console.log(`<< ${message}`)
        let lines = message.split('\n')

        let room_id = 'global'
        if (lines[0].startsWith('>')) {
            room_id = lines[0].substring(1)
            lines = lines.slice(1)
        }

        for (let line of lines) {
            if (line.startsWith('|')) {
                line = line.substring(1)
                const parts = line.split('|')
                if (parts.length === 1 && parts[0] === '') continue
                this.run(room_id, ...parts)
            } else {
                this.run(room_id, 'log', line)
            }
        }
    }

    search(format) {
        this.send(`/search ${format}`)
    }

    challenge(username, format) {
        this.send(`/challenge ${username}, ${format}`)
    }

    join(room_id) {
        this.send(`/join ${room_id}`)
    }

    login(username, assertion) {
        this.send(`/trn ${username},0,${assertion}`)
    }

    logout() {
        this.send('/logout')
    }
}
