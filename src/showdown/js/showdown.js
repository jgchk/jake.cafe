import '../css/sdmc.css'
import Tabs from './tabs.js'
import Socket from './socket.js'
import Session from './session.js'
import GlobalRoom from './tabs/global.js'

export const socket = new Socket()
export const tabs = new Tabs()
export const global = new GlobalRoom()
tabs.add_tab(global)
export const session = new Session()
