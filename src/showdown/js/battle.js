import '../css/battle.css'
import { session } from './showdown.js'

export default class Battle {
    constructor() {
        this.players = {}
    }

    get me() {
        return Object.values(this.players).filter(
            player => player.username === session.username
        )[0]
    }

    get opponent() {
        return Object.values(this.players).filter(
            player => player.username !== session.username
        )[0]
    }

    add_player(player, username, avatar) {
        const p = this.players[player]
            ? this.players[player]
            : new Player(username, avatar)
        if (username) p.username = username
        if (avatar) p.avatar = avatar
        this.players[player] = p
        return p
    }
}

class Player {
    constructor(username, avatar) {
        this.username = username
        this.avatar = avatar
        this.pokemon = {}
    }

    get active() {
        return Object.values(this.pokemon).filter(mon => mon.active)[0]
    }

    add_pokemon(name, species, shiny, gender, level) {
        const mon = this.pokemon[name]
            ? this.pokemon[name]
            : new Pokemon(name, species, shiny, gender, level)
        if (name) mon.name = name
        if (species) mon.species = species
        if (shiny != null) mon.shiny = shiny
        if (gender) mon.gender = gender
        if (level) mon.level = level
        this.pokemon[name] = mon
        return mon
    }
}

class Pokemon {
    constructor(name, species, shiny, gender, level) {
        this.name = name
        this.species = species
        this.shiny = shiny
        this.gender = gender
        this.level = level
        this.hp_pct = 1
        this.status = null
        this.moves = {}
        this.stats = {}
        this.base_ability = null
        this.ability = null
        this.item = null
        this.pokeball = null
        this.active = false
    }

    add_move(move) {
        move = move
            .toLowerCase()
            .replace('-', '')
            .replace(' ', '')
        const m = this.moves[move] ? this.moves[move] : new Move(move)
        if (move) m.name = move
        this.moves[move] = m
        return m
    }

    set_condition(hp, total_hp, status) {
        this.hp_pct = hp / total_hp || 0
        this.status = status
    }

    set_stats(stats) {
        for (const stat of Object.keys(stats))
            if (stats[stat] != null) this.stats[stat] = stats[stat]
    }
}

class Move {
    constructor(name) {
        this.name = name
    }
}
