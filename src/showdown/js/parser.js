export default class Parser {
    constructor(battle) {
        this.battle = battle
    }

    handlers = {
        player: (player, username, avatar) => {
            this.battle.add_player(player, username, avatar)
        },
        move: (pokemon, move, target) => {
            const [player, name] = parse_pokemon(pokemon)
            this.battle.players[player].pokemon[name].add_move(move)
        },
        switch: (pokemon, details, hp_status) =>
            this.switchlike(pokemon, details, hp_status),
        drag: (pokemon, details, hp_status) =>
            this.switchlike(pokemon, details, hp_status),
        detailschange: (pokemon, details, hp_status) =>
            this.switchlike(pokemon, details, hp_status),
        '-formechange': (pokemon, details, hp_status) =>
            this.switchlike(pokemon, details, hp_status),
        replace: (pokemon, details, hp_status) =>
            this.switchlike(pokemon, details, hp_status),
        faint: pokemon => {
            const [player, name] = parse_pokemon(pokemon)
            this.battle.players[player].pokemon[name].set_condition(
                0,
                null,
                'fnt'
            )
        },
        '-damage': (pokemon, hp_status) => this.damagelike(pokemon, hp_status),
        '-heal': (pokemon, hp_status) => this.damagelike(pokemon, hp_status),
        request: data => {
            data = JSON.parse(data)
            const side = data['side']
            const name = side['name']
            const id = side['id']
            this.battle.add_player(id, name)

            for (const pokemon of side['pokemon']) {
                const [player, name] = parse_pokemon(pokemon['ident'])
                const [species, level, gender, shiny] = parse_details(
                    pokemon['details']
                )
                const [hp, total_hp, status] = parse_hp_status(
                    pokemon['condition']
                )
                const stats = pokemon['stats']
                if (total_hp) stats['hp'] = total_hp

                const mon = this.battle.players[player].add_pokemon(
                    name,
                    species,
                    shiny,
                    gender,
                    level
                )
                mon.set_condition(hp, total_hp, status)
                for (const move of pokemon['moves']) mon.add_move(move)
                mon.set_stats(stats)
                mon.base_ability = pokemon['baseAbility']
                mon.ability = pokemon['ability']
                mon.item = pokemon['item']
                mon.pokeball = pokemon['pokeball']
                mon.active = pokemon['active']
            }
        },
    }

    switchlike(pokemon, details, hp_status) {
        const [player, name] = parse_pokemon(pokemon)
        const [species, level, gender, shiny] = parse_details(details)
        const [hp, total_hp, status] = parse_hp_status(hp_status)

        const mon = this.battle.players[player].add_pokemon(
            name,
            species,
            shiny,
            gender,
            level
        )
        mon.set_condition(hp, total_hp, status)
    }

    damagelike(pokemon, hp_status) {
        const [player, name] = parse_pokemon(pokemon)
        const [hp, total_hp, status] = parse_hp_status(hp_status)
        this.battle.players[player].pokemon[name].set_condition(
            hp,
            total_hp,
            status
        )
    }
}

function parse_pokemon(pokemon) {
    const pokemon_data = pokemon.split(': ')
    const player = pokemon_data[0].substring(0, 2)
    const name = pokemon_data[1]
    return [player, name]
}

function parse_details(details) {
    const details_data = details.split(', ').reverse()
    const species = details_data.pop()
    const level =
        details_data.length &&
        details_data[details_data.length - 1].startsWith('L')
            ? parseInt(details_data.pop().substring(1))
            : 100
    const gender =
        details_data.length && details_data[details_data.length - 1] !== 'shiny'
            ? details_data.pop()
            : 'N'
    const shiny = details_data.length ? details_data.pop() === 'shiny' : false

    return [species, level, gender, shiny]
}

function parse_hp_status(hp_status) {
    const [hp_str, status] = hp_status.split(' ')
    const hp_data = hp_str.split('/')
    const hp = parseInt(hp_data[0])
    const total_hp = parseInt(hp_data[1])
    return [hp, total_hp, status]
}
