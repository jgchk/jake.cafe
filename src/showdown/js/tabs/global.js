import $ from 'jquery'
import BattleRoom from './battle.js'
import Room from './room.js'
import { socket, tabs } from '../showdown.js'
import '../../css/global.css'
import Replays from './replays.js'

export default class GlobalRoom extends Room {
    constructor() {
        super('home', 'global', 'global-tab')
        this.on('updatesearch', state => {
            state = JSON.parse(state)
            if (state.games)
                for (const room_id of Object.keys(state.games))
                    tabs.add_tab(new BattleRoom(room_id))
        })
    }

    setup() {
        super.setup()
        this.setup_menu()
    }

    setup_menu() {
        const menu = $('#menu')

        const format_select = menu.find('#format')
        const battle_btn = menu.find('#battle')
        const challenge_input = menu.find('#challenger')
        const challenge_btn = menu.find('#challenge')
        const replays_btn = menu.find('#replays')

        battle_btn.on('click', () => {
            const format = format_select.val()
            socket.search(format)
        })

        challenge_btn.on('click', () => {
            const format = format_select.val()
            const opponent = challenge_input.val()
            socket.challenge(opponent, format)
        })

        replays_btn.on('click', () =>
            tabs.add_tab(new Replays(format_select.val()))
        )
    }
}
