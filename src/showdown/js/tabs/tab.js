import $ from 'jquery'
import { tabs } from '../showdown.js'
import EventDispatcher from '../../../common/js/event_dispatcher.js'

export default class Tab extends EventDispatcher {
    constructor(title, id, template_id) {
        super()

        this.title = title
        this.id = id
        this.template_id = template_id

        this.load()
    }

    load() {
        const tablink_template = $('#tablink')
        this.tablink = $(tablink_template.html())
        this.tablink.attr('id', this.id)
        this.tablink.find('.title').text(this.title)
        this.tablink.find('.close').on('click', () => tabs.remove_tab(this))
        this.tablink.on('click', () => this.activate())
        $('#tabs').append(this.tablink)

        const template = $(`#${this.template_id}`)
        this.tab = $(template.html())
        this.tab.attr('id', this.id)
        $('#tab-container').append(this.tab)

        this.setup()
    }

    setup() {}

    activate() {
        $('#tabs')
            .find('.active')
            .removeClass('active')
        this.tablink.addClass('active')

        $('#tab-container')
            .find('.active')
            .removeClass('active')
        this.tab.addClass('active')
    }

    close() {
        this.tablink.remove()
        this.tab.remove()
    }
}
