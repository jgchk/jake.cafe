import Battle from '../battle.js'
import Parser from '../parser.js'
import Room from './room.js'
import $ from 'jquery'

export default class BattleRoom extends Room {
    constructor(room_id) {
        super(room_id, room_id, 'battle-tab')
        this.battle = new Battle()
        this.parser = new Parser(this.battle)

        for (const handler of Object.keys(this.parser.handlers))
            this.on(handler, (...args) => {
                this.parser.handlers[handler](...args)
                this.show_state()
            })
    }

    show_state() {
        const display = $(`#${this.id} .content .state`)
        display.text(JSON.stringify(this.battle, null, 2))
    }

    show_context() {
        const context = {
            moves: rank_moves(this.battle.me.active, this.battle.opponent.active)
        }
    }
}
