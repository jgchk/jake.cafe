import $ from 'jquery'
import replays_db from '../replays_db.js'
import Tab from './tab.js'
import '../../css/replays.css'

export default class Replays extends Tab {
    constructor(format) {
        super('replays', `${format}-replays`, 'replays-tab')
        this.format = format

        replays_db.all(this.format).then(battles => {
            for (const battle of battles) this.add_replay(battle)
        })
    }

    setup() {
        super.setup()

        const refresh_btn = $(`#${this.id} .refresh`)
        refresh_btn.on('click', () => {
            fetch(`/showdown/replays/${this.format}`)
                .then(response => response.json())
                .then(replay_ids => {
                    for (const replay_id of replay_ids.reverse())
                        replays_db.add(replay_id).then(replay => {
                            if (replay) this.add_replay(replay)
                        })
                })
        })
    }

    add_replay(battle) {
        const battle_btn = $(
            `<div class="replay" id="${battle.id}">${battle.id}</div>`
        )
        battle_btn.on('click', () => {
            $(`#${this.id} .replay.selected`).removeClass('selected')
            battle_btn.addClass('selected')
            this.select_replay(battle)
        })
        $(`#${this.id} .list`).prepend(battle_btn)
    }

    select_replay(battle) {
        const log = $(`#${this.id} .log`)
        log.empty()
        for (const line of battle.log.split('\n'))
            log.append($(`<div class="line">${line}</div>`))
    }
}
