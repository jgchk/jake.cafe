import $ from 'jquery'
import '../../css/tab.css'
import { socket } from '../showdown.js'
import Tab from './tab.js'

export default class Room extends Tab {
    constructor(title, room_id, template_id) {
        super(title, room_id, template_id)
        this.on('popup', msg => alert(msg))
        socket.on(this.id, (type, ...args) => {
            this.log(type, ...args)
            this.run(type, ...args)
        })
    }

    setup() {
        super.setup()
        this.setup_cmd()
    }

    setup_cmd() {
        const input = $(`#${this.id} .commandline`)
        const send_btn = $(`#${this.id} .send-command`)

        input.on('keyup', e => {
            if (e.keyCode === 13) send_btn.click()
        })

        send_btn.on('click', () => {
            socket.send(input.val(), this.id)
            input.val('')
        })
    }

    log(type, ...args) {
        const log = $(`#${this.id} .log`)
        const scrolled_to_bottom = is_scrolled_to_bottom()
        let message =
            type === 'log' ? args.join('|') : `|${type}|${args.join('|')}`
        log.append($(`<div class="message">${message}</div>`))
        if (scrolled_to_bottom) scroll_to_bottom()

        function is_scrolled_to_bottom() {
            return (
                log[0].scrollHeight - log[0].clientHeight <=
                log[0].scrollTop + 1
            )
        }

        function scroll_to_bottom() {
            log[0].scrollTop = log[0].scrollHeight - log[0].clientHeight
        }
    }
}
