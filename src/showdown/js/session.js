import $ from 'jquery'
import EventDispatcher from '../../common/js/event_dispatcher.js'
import { global, socket } from './showdown.js'

export default class Session extends EventDispatcher {
    constructor() {
        super()

        this.username = null
        this.challstr = null
        this.logged_in = false

        global.on('challstr', (...args) => this.on_challstr(...args))
        global.on('updateuser', (...args) => this.on_updateuser(...args))

        this.setup_auth_ui()
    }

    get logged_in() {
        return this._logged_in
    }
    set logged_in(val) {
        this._logged_in = val
        if (val) this.run('login', this.username)
        else this.run('logout')
    }

    on_challstr(challstr1, challstr2) {
        this.challstr = [challstr1, challstr2].join('|')
    }

    on_updateuser(username, named, avatar, settings) {
        this.username = username
        this.logged_in = named === '1'
    }

    login(username) {
        fetch('/showdown/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                userid: username,
                challstr: this.challstr,
            }),
        })
            .then(response => response.text())
            .then(assertion => socket.login(username, assertion))
    }

    logout() {
        fetch('/showdown/logout', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ userid: this.username }),
        }).then(response => {
            if (response.status === 200) socket.logout()
        })
    }

    setup_auth_ui() {
        const login_wrapper = $('#login')
        const username_input = $('#login .username')
        const login_btn = $('#login button')
        const logout_wrapper = $('#logout')
        const username_display = $('#logout .username')
        const logout_btn = $('#logout button')

        username_input.on('keyup', e => {
            if (e.keyCode === 13) login_btn.click()
        })

        login_btn.on('click', () => this.login(username_input.val()))

        this.on('login', username => {
            login_wrapper.css('display', 'none')
            logout_wrapper.css('display', 'flex')
            username_display.text(username)
        })

        logout_btn.on('click', () => this.logout())

        this.on('logout', () => {
            logout_wrapper.css('display', 'none')
            login_wrapper.css('display', 'flex')
        })
    }
}
