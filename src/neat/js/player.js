import { mouse } from './field.js'

export default class Player {
    constructor() {
        this.x = 0
        this.y = 0
        this.r = 50
    }

    update() {
        this.x = mouse.x
        this.y = mouse.y
    }
}
