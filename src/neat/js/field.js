import Two from './lib/two.min.js'
import Mouse from '../../common/js/mouse.js'
import { player, enemies, new_generation } from './neat.js'

export const WIDTH = 500
export const HEIGHT = 500

const elem = document.getElementById('draw-shapes')
const params = { width: WIDTH, height: HEIGHT }
const two = new Two(params).appendTo(elem)

export const mouse = new Mouse('mousemove')
mouse.update = e => {
    mouse.x = e.clientX - elem.getBoundingClientRect().left
    mouse.y = e.clientY - elem.getBoundingClientRect().top
}

export function setup() {
    player.elem = two.makeCircle(player.x, player.y, player.r)
    player.elem.fill = '#FF8000'
    player.elem.stroke = 'orangered'
    player.elem.linewidth = 5
    player.elem.translation = new Two.Vector(0, 0)

    setup_enemies()

    two.update()
    two.bind('update', update).play()
}

export function setup_enemies() {
    for (const enemy of enemies) {
        enemy.elem = two.makeCircle(enemy.x, enemy.y, enemy.r)
        enemy.elem.fill = '#FFFFFF'
        enemy.elem.stroke = 'black'
        enemy.elem.linewidth = 5
        enemy.elem.translation = new Two.Vector(0, 0)
    }
}

function update() {
    player.update()
    player.elem.translation.x = player.x
    player.elem.translation.y = player.y

    for (const enemy of enemies) {
        enemy.update()
        if (enemy.dead) {
            if (enemy.elem !== undefined) {
                two.remove(enemy.elem)
                delete enemy.elem
            }
        } else {
            enemy.elem.translation.x = enemy.x
            enemy.elem.translation.y = enemy.y
        }
    }

    if (!this.finished && enemies.every(enemy => enemy.dead)) {
        new_generation()
    }
}
