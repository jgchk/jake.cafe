import { player } from './neat.js'
import { WIDTH, HEIGHT } from './field.js'

const MAX_SPEED = 5

export default class Enemy {
    constructor(genome) {
        this.x = random(0, WIDTH)
        this.y = random(0, HEIGHT)
        this.vx = 0
        this.vy = 0
        this.r = 10
        this.dead = false

        this.brain = genome
        this.brain.score = 0
    }

    update() {
        if (!this.dead && this.dist_to_player() <= this.r + player.r)
            this.dead = true
        if (this.dead) return

        const input = this.detect()
        const output = this.brain.activate(input)

        const move_angle = output[0] * 2 * Math.PI

        // Calculate next position
        const ax = Math.cos(move_angle)
        const ay = Math.sin(move_angle)
        this.vx += ax
        this.vy += ay

        // Limit speeds to max speed
        this.vx = clamp(this.vx, -MAX_SPEED, MAX_SPEED)
        this.vy = clamp(this.vy, -MAX_SPEED, MAX_SPEED)

        this.x += this.vx
        this.y += this.vy

        // Wrap around
        if (this.x > WIDTH) this.x -= WIDTH
        else if (this.x < 0) this.x = WIDTH + this.x
        if (this.y > HEIGHT) this.y -= HEIGHT
        else if (this.y < 0) this.y = HEIGHT + this.y

        this.score()
    }

    detect() {
        let dist = this.dist_to_player() / Math.hypot(WIDTH, HEIGHT)
        let target_angle =
            angle_to_point(this.x, this.y, player.x, player.y) / (2 * Math.PI)
        const vx = (this.vx + MAX_SPEED) / MAX_SPEED
        const vy = (this.vy + MAX_SPEED) / MAX_SPEED

        // NaN checking
        target_angle = isNaN(target_angle) ? 0 : target_angle
        dist = isNaN(dist) ? 0 : dist

        return [vx, vy, target_angle, dist]
    }

    score() {
        const dist = this.dist_to_player()
        if (!isNaN(dist) && dist > this.r + player.r) {
            this.brain.score += dist - (this.r + player.r)
        }
    }

    dist_to_player() {
        return Math.hypot(player.x - this.x, player.y - this.y)
    }
}

function angle_to_point(x1, y1, x2, y2) {
    const dist = Math.hypot(x2 - x1, y2 - y1)
    const dx = (x2 - x1) / dist
    const dy = (y2 - y1) / dist

    let a = Math.acos(dx)
    a = dy < 0 ? 2 * Math.PI - a : a
    return a
}

function clamp(n, min, max) {
    return n < min ? min : n > max ? max : n
}

function random(min, max) {
    return Math.floor(Math.random() * max + min)
}
