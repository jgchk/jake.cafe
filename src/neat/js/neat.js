import '../css/neat.css'
import { Neat, methods } from 'neataptic'
import Player from './player.js'
import Enemy from './enemy.js'
import { setup as setup_field, setup_enemies } from './field.js'

const neat = new Neat(4, 1, null, {
    mutation: methods.mutation.FFW,
    popsize: 50,
    mutationRate: 0.5,
    elitism: 10,
})

export let enemies = neat.population.map(genome => new Enemy(genome))
export const player = new Player()

setup_field()

export function new_generation() {
    neat.sort()

    const new_population = []
    for (let i = 0; i < neat.elitism; i++)
        new_population.push(neat.population[i])

    for (let i = 0; i < neat.popsize - neat.elitism; i++)
        new_population.push(neat.getOffspring())

    neat.population = new_population
    neat.mutate()
    neat.generation++

    enemies = neat.population.map(genome => new Enemy(genome))
    setup_enemies()
}
