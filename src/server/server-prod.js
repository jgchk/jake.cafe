/* eslint-disable no-console */
import express from 'express'
import routes from './routes/routes.js'

const app = express()

app.use('/', routes)

const PORT = process.env.PORT || 8080
app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`)
    console.log('Press Ctrl+C to quit.')
})
