import { Router } from 'express'
import path from 'path'
import fetch from 'node-fetch'

const bc = Router(),
    DIST_DIR = __dirname,
    BANDCAMP_VIEW = path.join(DIST_DIR, 'bandcamp.html')

bc.get('/', (req, res) => res.sendFile(BANDCAMP_VIEW))

bc.get('/data/:url', (req, res) => {
    const url = req.params.url
    fetch(url)
        .then(response => response.text())
        .then(text => {
            const data = text.match(/var TralbumData = ({(.|\n)*?});/)
            const album_data = eval(`(${data[1]})`)
            res.send(album_data)
        })
})

export default bc
