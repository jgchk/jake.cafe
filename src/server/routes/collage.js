import body_parser from 'body-parser'
import express, { Router } from 'express'
import path from 'path'
import Hashids from 'hashids'
import multer from 'multer'
import FileSync from 'lowdb/adapters/FileSync'
import low from 'lowdb'

const hashids = new Hashids('collage', 5)

const adapter = new FileSync('db.json')
const db = low(adapter)
db.defaults({ collages: [], count: 0 }).write()

const collage = Router(),
    DIST_DIR = __dirname,
    COLLAGE_VIEW = path.join(DIST_DIR, 'collage.html'),
    UPLOADS_DIR = path.join(DIST_DIR, '../uploads'),
    upload = multer({ dest: UPLOADS_DIR })

collage.use(express.static(UPLOADS_DIR))
collage.use(body_parser.json())

collage.get('/', (req, res) => res.sendFile(COLLAGE_VIEW))

collage.post('/', (req, res) => {
    if (req.body.length === 0) {
        res.send({ success: false, error: "Can't share an empty collage :(" })
    } else {
        const id = db.get('count').value()
        db.get('collages')
            .push(req.body)
            .write()
        db.update('count', n => n + 1).write()
        res.send({ success: true, id: hashids.encode(id) })
    }
})

collage.post('/upload', upload.single('file'), (req, res) => res.send(req.file))

collage.get('/:id', (req, res) => res.sendFile(COLLAGE_VIEW))

collage.post('/:id', (req, res) => {
    const collage_index = hashids.decode(req.params.id)[0]
    const collage = db.get(`collages[${collage_index}]`).value()
    res.send(collage)
})

export default collage
