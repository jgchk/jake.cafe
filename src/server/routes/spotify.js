import { Router } from 'express'
import qs from 'querystring'
import request from 'request'
import url from 'url'
import cookie_parser from 'cookie-parser'
import path from 'path'

const spotify = Router(),
    DIST_DIR = __dirname,
    SPOTIFY_VIEW = path.join(DIST_DIR, 'spotify.html')

const client_id = 'fae5b2297a294cb5b38887eed5ba8032'
const client_secret = '64d05cf9f63c4cf1a8670144f892c056'
const redirect_uri = '/spotify/callback'

const state_key = 'spotify_auth_state'
const from_key = 'from'

spotify.use(cookie_parser())

spotify.get('/', (req, res) => {
    const access_token = req.query.access_token
    if (!access_token) return res.redirect('/spotify/login?from=/spotify')
    return res.sendFile(SPOTIFY_VIEW)
})

spotify.get('/login', (req, res) => {
    const state = generate_random_string(16)
    res.cookie(state_key, state)

    const from = req.query.from
    if (from) res.cookie(from_key, from)

    const scope =
        'streaming user-read-birthdate user-read-email user-read-private'
    res.redirect(
        'https://accounts.spotify.com/authorize?' +
            qs.stringify({
                response_type: 'code',
                client_id: client_id,
                scope: scope,
                redirect_uri: full_url(req) + redirect_uri,
                state: state,
            })
    )
})

spotify.get('/callback', (req, res) => {
    const code = req.query.code || null
    const state = req.query.state || null
    const stored_state = req.cookies ? req.cookies[state_key] : null
    const from = req.cookies ? req.cookies[from_key] || '/' : '/'

    if (state === null || state !== stored_state) {
        res.redirect('/#' + qs.stringify({ error: 'state_mismatch' }))
    } else {
        res.clearCookie(state_key)
        res.clearCookie(from_key)
        const auth_options = {
            url: 'https://accounts.spotify.com/api/token',
            form: {
                code: code,
                redirect_uri: full_url(req) + redirect_uri,
                grant_type: 'authorization_code',
            },
            headers: {
                Authorization:
                    'Basic ' +
                    Buffer.from(client_id + ':' + client_secret).toString(
                        'base64'
                    ),
            },
            json: true,
        }

        request.post(auth_options, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                const access_token = body.access_token
                const refresh_token = body.refresh_token

                const options = {
                    url: 'https://api.spotify.com/v1/me',
                    headers: { Authorization: 'Bearer ' + access_token },
                    json: true,
                }

                request.get(options, (error, response, body) =>
                    // eslint-disable-next-line no-console
                    console.log(body)
                )
                res.redirect(
                    `${from}?` +
                        qs.stringify({
                            access_token: access_token,
                            refresh_token: refresh_token,
                        })
                )
            } else {
                res.redirect(from + qs.stringify({ error: 'invalid_token' }))
            }
        })
    }
})

spotify.get('/refresh_token', (req, res) => {
    const refresh_token = req.query.refresh_token
    const auth_options = {
        url: 'https://accounts.spotify.com/api/token',
        headers: {
            Authorization:
                'Basic ' +
                Buffer.from(client_id + ':' + client_secret).toString('base64'),
        },
        form: {
            grant_type: 'refresh_token',
            refresh_token: refresh_token,
        },
        json: true,
    }

    request.post(auth_options, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            const access_token = body.access_token
            res.send({ access_token: access_token })
        }
    })
})

function full_url(req) {
    return url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.originalURL,
    })
}

function generate_random_string(length) {
    let text = ''
    const alphabet =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (let i = 0; i < length; i++)
        text += alphabet.charAt(Math.floor(Math.random() * alphabet.length))
    return text
}

export default spotify
