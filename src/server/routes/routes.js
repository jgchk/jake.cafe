import express, { Router } from 'express'
import path from 'path'
import spotify from './spotify.js'
import bandcamp from './bandcamp.js'
import collage from './collage.js'
import neat from './neat.js'
import showdown from './showdown.js'

const routes = Router(),
    DIST_DIR = __dirname,
    INDEX_VIEW = path.join(DIST_DIR, 'index.html')

routes.use(express.static(DIST_DIR))

routes.get('/', (req, res) => res.sendFile(INDEX_VIEW))

routes.use('/collage', collage)
routes.use('/spotify', spotify)
routes.use('/bandcamp', bandcamp)

routes.use('/neat', neat)

routes.use('/showdown', showdown)

export default routes
