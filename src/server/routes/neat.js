import { Router } from 'express'
import path from 'path'

const neat = Router(),
    DIST_DIR = __dirname,
    NEAT_VIEW = path.join(DIST_DIR, 'neat.html')

neat.get('/', (req, res) => res.sendFile(NEAT_VIEW))

export default neat
