import express, { Router } from 'express'
import path from 'path'
import request from 'request'
import fetch from 'node-fetch'
import qs from 'querystring'

const showdown = Router(),
    DIST_DIR = __dirname,
    SHOWDOWN_VIEW = path.join(DIST_DIR, 'showdown.html')

showdown.use(express.json())

showdown.get('/', (req, res) => res.sendFile(SHOWDOWN_VIEW))

showdown.post('/login', (req, res) => {
    fetch(
        'https://play.pokemonshowdown.com/action.php?' +
            qs.stringify({
                act: 'getassertion',
                userid: req.body.userid,
                challstr: req.body.challstr,
            }),
        {
            method: 'POST',
        }
    )
        .then(response => response.text())
        .then(text => res.send(text))
})

showdown.post('/logout', (req, res) => {
    fetch(
        'https://play.pokemonshowdown.com/action.php?' +
            qs.stringify({
                act: 'logout',
                userid: req.body.userid,
            }),
        {
            method: 'POST',
        }
    ).then(response => res.sendStatus(response.status))
})

showdown.get('/replays/:format', (req, res) => {
    request(
        `https://replay.pokemonshowdown.com/search/?format=${
            req.params.format
        }`,
        (error, response, body) => {
            const battles = body.match(
                new RegExp(`${req.params.format}-\\d+`, 'g')
            )
            res.send(battles)
        }
    )
})

showdown.get('/replay/:id', (req, res) => {
    request(
        `https://replay.pokemonshowdown.com/${req.params.id}.log`,
        (error, response, body) => res.send(body)
    )
})

export default showdown
