//////////////////////////////
//    _____ _ _ _ _____     //
//   |     | | | |     |    //
//   |_|_|_|_____|_|_|_|    //
//  mocha's window manager  //
//                          //
//////////////////////////////

import $ from 'jquery'
import mouse_tracker from '../../common/js/mouse.js'
import text_fit from './lib/textFit.min.js'
import setup_drag from './draggable.js'
import setup_resizer from './resizable.js'
import '../css/mwm.css'

class MWM {
    handle_height = 30

    constructor(container_id, converters) {
        this.$container = $(`#${container_id}`)
        this.converters = converters || {}
        this.current_win_id = 0
        this.mouse = new mouse_tracker('mousemove', 'drop')
        this.windows = {}
    }

    create_window(win_data, x = this.mouse.x, y = this.mouse.y) {
        win_data.x = x
        win_data.y = y
        this.add_window(win_data)
    }

    add_window(win_data) {
        const $el = this.converters[win_data.type].render(win_data, 'window')
        $el.addClass('element')

        if (win_data.width != null && win_data.height != null)
            setup(this, win_data.width, win_data.height)
        else {
            // Get default element width/height
            this.$container.append($el)
            if ($el[0] instanceof HTMLImageElement) {
                let loaded = false
                $el.on('load', () => {
                    if (!loaded) setup(this)
                    loaded = true
                })
            } else {
                setup(this)
            }
        }

        function setup(self, width, height) {
            if (width == null) width = $el.width()
            if (height == null) height = $el.height() + self.handle_height

            $el.width('')
            $el.height('')

            const id = self.current_win_id
            const win_el = self.make_window($el, id)
            self.$container.append(win_el)

            self.windows[id] = new win(win_data, win_el)
            self.set_size(id, width, height)
            self.set_pos(id, win_data.x, win_data.y)
            self.current_win_id++
        }
    }

    make_window($element, id) {
        const $win = $(`<div id="${MWM.to_win_id(id)}" class="window"></div>`)
        const $handle = $('<div class="handle decoration"></div>')

        const $forward_btn = $('<span class="forward button">↑</span>')
        $forward_btn.on('click', () => $win.insertAfter($win.next()))
        $handle.append($forward_btn)

        const $backward_btn = $('<span class="backward button">↓</span>')
        $backward_btn.on('click', () => $win.insertBefore($win.prev()))
        $handle.append($backward_btn)

        const $close_btn = $('<span class="close button">×</span>')
        $close_btn.on('click', () => this.remove_window(id))
        $handle.append($close_btn)

        const $content = $('<div class="content"></div>')
        $content.append($element)

        const $resizers = $('<div class="resizers decoration"></div>')
        for (const resizer_class of [
            'top-left',
            'top-right',
            'bottom-left',
            'bottom-right',
        ]) {
            const $resizer = $(`<div class="resizer ${resizer_class}"></div>`)
            $resizers.append($resizer)

            setup_resizer($resizer, $win, this.mouse, r => {
                this.set_size(id, r.width, r.height)
                this.set_pos(id, r.x, r.y)
            })
        }

        $win.append($resizers)
        $win.append($handle)
        $win.append($content)

        setup_drag($win, pos => this.set_pos(id, pos.x, pos.y))

        //<editor-fold defaultstate="collapsed" desc="setup_resizing(win)">
        $win.on('mouseenter mousemove', () => {
            const $resizers = $win.find('.resizer')
            for (const resizer of $resizers) {
                const $resizer = $(resizer)
                const dist_to_mouse = distance(
                    this.mouse.x,
                    this.mouse.y,
                    $resizer.offset().left + $resizer.width() / 2,
                    $resizer.offset().top + $resizer.height() / 2
                )
                const size =
                    2 * Math.round(sigmoid(-1 - dist_to_mouse / 30) * 25 + 6)
                $resizer.width(size)
                $resizer.height(size)
            }

            function distance(a_x, a_y, b_x, b_y) {
                return Math.sqrt(
                    Math.pow(a_x - b_x, 2) + Math.pow(a_y - b_y, 2)
                )
            }

            function sigmoid(t) {
                return 1 / (1 + Math.pow(Math.E, -t))
            }
        })
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="setup_editing(win)">
        if ($element.attr('contenteditable') === 'true')
            $content.on('blur cut copy paste delete keyup mouseup', () => {
                const $editable = $content.find('[contenteditable="true"]')
                const win = this.windows[id]
                this.converters[win.data.type].edit($editable.text(), win)
                text_fit($content)
            })
        //</editor-fold>

        return $win
    }

    add_element(win_data) {
        const id = this.current_win_id
        const $el = this.converters[win_data.type].render(win_data, 'element')
        $el[0].id = `win_${id}`
        $el.addClass('element')
        $el.addClass('content')
        $el.css('position', 'absolute')

        this.$container.append($el)

        this.windows[id] = new win(win_data, $el)
        this.set_size(id, win_data.width, win_data.height - this.handle_height)
        this.set_pos(id, win_data.x, win_data.y + this.handle_height)
        this.current_win_id++

        return $el
    }

    set_size(win_id, width, height) {
        const win = this.windows[win_id]
        win.data.width = width
        win.data.height = height
        win.$window.width(width)
        win.$window.height(height)
        if (win.$content.hasClass('text') || win.$content.find('.text').length)
            text_fit(win.$content)
    }

    set_pos(win_id, x, y) {
        const win = this.windows[win_id]
        win.data.x = x
        win.data.y = y
        win.$window.css('left', x)
        win.$window.css('top', y)
    }

    remove_window(win_id) {
        this.windows[win_id].$window.remove()
        delete this.windows[win_id]
    }

    clear() {
        this.current_win_id = 0
        for (let win of this.$container.children()) {
            const id = MWM.from_win_id(win.id)
            this.remove_window(id)
        }
    }

    export() {
        const export_windows = []
        for (let win of this.$container.children()) {
            const id = MWM.from_win_id(win.id)
            const data = this.windows[id].data
            export_windows.push(this.converters[data.type].save(data))
        }
        return export_windows
    }

    sorted_audio_ids() {
        return Object.keys(this.windows)
            .filter(key => this.windows[key].data.playable)
            .sort((a, b) => {
                const a_height = this.windows[a].data.y
                const b_height = this.windows[b].data.y
                return a_height - b_height
            })
    }

    static to_win_id(num) {
        return '#win_' + num
    }

    static from_win_id(id) {
        return parseInt(id.replace('#', '').replace('win_', ''))
    }
}

class win {
    constructor(data, $element) {
        this.data = data
        this.$window = $element
        this.$content = $element.hasClass('content')
            ? $element
            : $element.find('.content')
    }

    $element() {
        return this.$content.find('.element')
    }
}

export default MWM
