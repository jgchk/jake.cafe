/* eslint-disable no-console */
import Dropbox from './dropbox.js'
import MWM from './mwm.js'
import converters, { parse } from './converters.js'
import m from '../../common/js/mquery.js'
import '../css/collage.css'
import $ from 'jquery'

class Collage {
    saved_data = {
        get collage() {
            const sc = localStorage.getItem('saved_collage')
            if (sc != null && sc !== '') return JSON.parse(sc)
            return null
        },
        set collage(val) {
            localStorage.setItem('saved_collage', JSON.stringify(val))
        },
    }

    constructor() {
        this.wm = new MWM('container', converters)
        this.$context_links = $('#context.links')
        $('#home').on(
            'click',
            () => (window.location.href = window.location.origin)
        )

        const collage_id = Collage.collage_id_in_url()
        if (collage_id) this.load_collage(collage_id)
        else this.load_workspace()
    }

    static collage_id_in_url() {
        const path = window.location.pathname.split('/')
        if (path.length > 2) return path[2]
        return false
    }

    load_collage(collage_id) {
        this.wm.clear()
        return fetch(`/collage/${collage_id}`, { method: 'POST' })
            .then(response => response.json())
            .then(json => {
                let playable = false
                for (const win_data of json) {
                    const $el = this.wm.add_element(win_data)

                    if (win_data.playable) {
                        playable = true
                        $el.on('play', () => {
                            const win_num = String(MWM.from_win_id($el[0].id))
                            this.currently_playing_index = this.play_order.indexOf(
                                win_num
                            )
                            this.playing = true
                        })
                        $el.on('pause', () => (this.playing = false))
                        $el.on('ended', () =>
                            this.play(++this.currently_playing_index)
                        )
                    }
                }

                if (playable) {
                    this.play_order = this.wm.sorted_audio_ids()
                    this.currently_playing_index = 0

                    this.$autoplay_btn = $('<span>')
                    this.$autoplay_btn.on('click', () => {
                        if (this.playing) this.pause()
                        else this.play(this.currently_playing_index)
                    })
                    this.$context_links.append(this.$autoplay_btn)

                    this.playing = false
                }
            })
    }

    play(index) {
        if (index >= this.play_order.length) {
            this.currently_playing_index = 0
            this.playing = false
            return
        }

        const id_num = this.play_order[index]
        const $playing_el = $(MWM.to_win_id(id_num))

        if ($playing_el[0].ready) $playing_el[0].play()
        else $playing_el.on('ready', () => $playing_el[0].play())

        this.playing = true
    }

    pause() {
        const id_num = this.play_order[this.currently_playing_index]
        const playing_el = $(MWM.to_win_id(id_num))[0]
        playing_el.pause()

        this.playing = false
    }

    get playing() {
        return this.$autoplay_btn.hasClass('playing')
    }
    set playing(val) {
        if (val) {
            this.$autoplay_btn.addClass('playing')
            this.$autoplay_btn.text('pause')
        } else {
            this.$autoplay_btn.removeClass('playing')
            this.$autoplay_btn.text('play')
        }
    }

    load_workspace() {
        this.dropbox = new Dropbox(
            'dropbox',
            'dropbox__file',
            'progress-bar',
            file_data => this.handle_data(file_data),
            text => this.handle_data(text)
        )

        window.onunload = () => (this.saved_data.collage = this.wm.export())

        const $clear_btn = $('<span>clear</span>')
        const $share_btn = $('<span>share</span>')
        $clear_btn.on('click', () => this.clear())
        $share_btn.on('click', () => this.share())
        this.$context_links.append($clear_btn)
        this.$context_links.append($share_btn)

        $(window).scroll(function() {
            if (
                $(window).scrollTop() + $(window).height() ===
                $(document).height()
            ) {
                const scroll_pos = $(window).scrollTop()
                const container = $('#container')
                container.css('height', '+=10%')
                $(window).scrollTop(scroll_pos)
            }
        })

        this.load_saved_workspace()
    }

    load_saved_workspace() {
        m.with(this.saved_data.collage, data => {
            for (const win_data of data) this.wm.add_window(win_data)
        })
    }

    handle_data(data) {
        const win_data = parse(data)
        if (win_data) this.wm.create_window(win_data)
    }

    clear() {
        if (confirm('Are you sure you want to clear the collage?'))
            this.wm.clear()
    }

    share() {
        fetch('/collage', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.wm.export()),
        })
            .then(response => response.json())
            .then(json => {
                if (json.success) window.location.href = '/collage/' + json.id
                else alert(json.error)
            })
    }
}

new Collage()
