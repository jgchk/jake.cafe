import './players/spotify.js'
import './players/bandcamp.js'
import './players/soundcloud.js'
import './players/youtube.js'
import $ from 'jquery'
import { parse_uri as parse_spotify_uri } from './players/spotify.js'

const converters = {
    image: {
        parse: data => {
            if (
                typeof data === 'object' &&
                data.mimetype &&
                data.mimetype.split('/')[0] === 'image'
            )
                return {
                    type: 'image',
                    src: data.filename,
                }
            else return false
        },
        render: win_data => $(`<img src="${win_data.src}">`),
        save: win_data => win_data,
    },
    audio: {
        parse: data => {
            if (
                typeof data === 'object' &&
                data.mimetype &&
                data.mimetype.split('/')[0] === 'audio'
            )
                return {
                    type: 'audio',
                    src: data.filename,
                    mimetype: data.mimetype,
                    playable: true,
                }
            else return false
        },
        render: win_data => {
            const $player = $('<audio controls>')
            $player.append(
                $(`<source src="${win_data.src}" type="${win_data.mimetype}">`)
            )
            $player.width(400)
            $player.height(40)
            $player[0].ready = true
            return $player
        },
        save: win_data => win_data,
    },
    spotify: {
        parse: data => {
            if (typeof data !== 'string') return false
            const uri = parse_spotify_uri(data)
            if (uri)
                return {
                    type: 'spotify',
                    uri: data,
                    playable: true,
                }
            else return false
        },
        render: win_data => {
            const $player = $(
                document.createElement('div', { is: 'm-spotify' })
            )
            $player.attr('uri', win_data.uri)
            $player.width(300)
            $player.height(300)
            return $player
        },
        save: win_data => win_data,
    },
    bandcamp: {
        parse: data => {
            if (typeof data === 'string' && is_bandcamp_url(data))
                return {
                    type: 'bandcamp',
                    url: data,
                    playable: true,
                }
            else return false

            function is_bandcamp_url(str) {
                const pattern = new RegExp(
                    /^https?:\/\/[a-z0-9\\-]+?\.bandcamp\.com\/(album|track)\/[a-z0-9\\-]+?$/
                )
                return !!pattern.test(str)
            }
        },
        render: win_data => {
            const $player = $(
                document.createElement('div', { is: 'm-bandcamp' })
            )
            $player.attr('url', win_data.url)
            $player.width(300)
            $player.height(300)
            return $player
        },
        save: win_data => win_data,
    },
    soundcloud: {
        parse: data => {
            if (typeof data === 'string' && is_soundcloud_url(data))
                return {
                    type: 'soundcloud',
                    url: data,
                    playable: true,
                }
            else return false

            function is_soundcloud_url(str) {
                const pattern = new RegExp(
                    /^https?:\/\/(soundcloud\.com|snd\.sc)\/(.*)$/
                )
                return !!pattern.test(str)
            }
        },
        render: win_data => {
            const $player = $(
                document.createElement('div', { is: 'm-soundcloud' })
            )
            $player.attr('url', win_data.url)
            $player.width(300)
            $player.height(300)
            return $player
        },
        save: win_data => win_data,
    },
    youtube: {
        parse: data => {
            if (typeof data === 'string' && is_youtube_url(data))
                return {
                    type: 'youtube',
                    url: data,
                    playable: true,
                }
            else return false

            function is_youtube_url(str) {
                const pattern = new RegExp(
                    /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/
                )
                return !!pattern.test(str)
            }
        },
        make_win_data: data => ({
            type: 'youtube',
            url: data,
            playable: true,
        }),
        render: win_data => {
            const $player = $(
                document.createElement('div', { is: 'm-youtube' })
            )
            $player.attr('url', win_data.url)
            $player.width(640)
            $player.height(390)
            return $player
        },
        save: win_data => win_data,
    },
    link: {
        parse: data => {
            if (typeof data === 'string' && is_link(data))
                return {
                    type: 'link',
                    href: data,
                }
            else return false

            function is_link(str) {
                const pattern = new RegExp(
                    /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=+$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:www.|[-;:&=+$,\w]+@)[A-Za-z0-9.-]+)((?:\/[+~%/.\w-_]*)?\??(?:[-+=&;%@.\w_]*)#?(?:[\w]*))?)/
                )
                return !!pattern.test(str)
            }
        },
        render: (win_data, format) => {
            const $a = $(
                `<a href="${win_data.href}" class="text">${win_data.href}</a>`
            )
            if (format === 'window') $a.attr('contenteditable', true)
            return $a
        },
        edit: (edit_data, win) => {
            win.data.href = edit_data
            win.$element().attr('href', edit_data)
        },
        save: win_data => win_data,
    },
    text: {
        parse: data => {
            if (typeof data === 'string')
                return {
                    type: 'text',
                    text: data,
                }
            else return false
        },
        render: (win_data, format) => {
            const $span = $(`<span class="text">${win_data.text}</span>`)
            if (format === 'window') $span.attr('contenteditable', true)
            return $span
        },
        edit: (edit_data, win) => {
            win.data.text = edit_data
        },
        save: win_data => win_data,
    },
}

export default converters
export function parse(data) {
    const parsers = Object.values(converters).map(converter => converter.parse)
    for (let parser of parsers) {
        const result = parser(data)
        if (result) return result
    }
}
