import $ from 'jquery'

function setup_drag($el, on_drag) {
    let pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0
    const $handle = $el.find('.handle')
    const draggable = $handle.length ? $handle : $el
    draggable.on('mousedown', drag_mouse_down)

    const $blocker = $(`<div></div>`)
    $blocker.css('background', 'transparent')
    $blocker.css('position', 'absolute')
    $blocker.css('z-index', 999)
    $blocker.css('cursor', 'move')
    $blocker.css('width', '100%')
    $blocker.css('height', '100%')

    function drag_mouse_down(e) {
        if (e.target !== draggable[0]) return
        e.preventDefault()
        $el.append($blocker)

        pos3 = e.clientX
        pos4 = e.clientY
        document.addEventListener('mouseup', close_drag_element)
        document.addEventListener('mousemove', element_drag)
    }

    function element_drag(e) {
        e.preventDefault()
        pos1 = pos3 - e.clientX
        pos2 = pos4 - e.clientY
        pos3 = e.clientX
        pos4 = e.clientY
        $el.css('top', $el.offset().top - pos2)
        $el.css('left', $el.offset().left - pos1)
    }

    function close_drag_element() {
        $blocker.remove()
        document.removeEventListener('mouseup', close_drag_element)
        document.removeEventListener('mousemove', element_drag)
        on_drag({ x: $el.offset().left, y: $el.offset().top })
    }
}

export default setup_drag
