import $ from 'jquery'

function setup_resizer($resizer, $resizable, mouse, on_resize) {
    const $blocker = $('<div>')
    $blocker.css('background', 'transparent')
    $blocker.css('position', 'absolute')
    $blocker.css('z-index', 999)
    $blocker.css('cursor', 'move')
    $blocker.css('width', '100%')
    $blocker.css('height', '100%')

    $resizer.on('mousedown', e => {
        e.preventDefault()

        $resizable.append($blocker)

        const orig_width = $resizable.width()
        const orig_height = $resizable.height()
        const orig_x = $resizable.offset().left
        const orig_y = $resizable.offset().top
        const orig_mouse_x = mouse.x
        const orig_mouse_y = mouse.y

        document.addEventListener('mousemove', resize)
        document.addEventListener('mouseup', stop_resize)

        function resize(e) {
            const diff_x = mouse.x - orig_mouse_x
            const diff_y = mouse.y - orig_mouse_y

            let w = 0,
                h = 0,
                x,
                y
            if ($resizer.hasClass('bottom-right')) {
                w = orig_width + diff_x
                h = orig_height + diff_y
            } else if ($resizer.hasClass('bottom-left')) {
                w = orig_width - diff_x
                h = orig_height + diff_y
                x = orig_x + diff_x
            } else if ($resizer.hasClass('top-right')) {
                w = orig_width + diff_x
                h = orig_height - diff_y
                y = orig_y + diff_y
            } else if ($resizer.hasClass('top-left')) {
                w = orig_width - diff_x
                h = orig_height - diff_y
                x = orig_x + diff_x
                y = orig_y + diff_y
            }

            if (!e.ctrlKey) {
                const scale = Math.max(w / orig_width, h / orig_height)
                w = orig_width * scale
                h = orig_height * scale
                x = x == null ? orig_x : orig_x - (w - orig_width)
                y = y == null ? orig_y : orig_y - (h - orig_height)
            }

            if (x == null) x = orig_x
            if (y == null) y = orig_y

            on_resize({ width: w, height: h, x: x, y: y })
        }

        function stop_resize() {
            document.removeEventListener('mousemove', resize)
            $blocker.remove()
        }
    })
}

export default setup_resizer
