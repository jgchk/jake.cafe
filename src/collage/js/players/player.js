import $ from 'jquery'

export default class Player extends HTMLDivElement {
    constructor(track_title_prop) {
        super()
        this.track_title_prop = track_title_prop
    }

    get ready() {
        return this.$wrapper.hasClass('ready')
    }
    set ready(val) {
        if (val) {
            this.$wrapper.addClass('ready')
            this.run_event('ready')
        } else {
            this.$wrapper.removeClass('ready')
            this.run_event('not_ready')
        }
    }

    get playing() {
        return this.$wrapper.hasClass('playing')
    }
    set playing(val) {
        if (val) {
            this.$wrapper.addClass('playing')
            this.$progress_bar.attr('playing', true)
            this.clear_playing_track()
            this.set_playing_track()
            this.run_event('play')
        } else {
            this.$wrapper.removeClass('playing')
            this.$progress_bar.attr('playing', false)
            this.run_event('pause')
        }
    }

    get tracks() {
        return this._tracks
    }
    set tracks(val) {
        this._tracks = val

        this.$playlist.empty()
        val.forEach((track, i) => {
            const $track_wrapper = $(`<div class="track"></div>`)

            const $num = $(`<span class="num">${i + 1}</span>`)
            $track_wrapper.append($num)

            const $name = $(
                `<span class="name">${track[this.track_title_prop]}</span>`
            )
            $track_wrapper.append($name)

            $track_wrapper.on('click', () => {
                if (this.ready) this.play(i)
            })
            this.$playlist.append($track_wrapper)
        })

        this.current_track = 0
    }

    get current_track() {
        return this._current_track
    }
    set current_track(val) {
        this._current_track = val
    }

    get position() {
        return parseInt(this.$progress_bar.val())
    }
    set position(val) {
        this.$progress_bar.val(parseInt(val))
    }

    get duration() {
        return parseInt(this.$progress_bar.attr('max'))
    }
    set duration(val) {
        this.$progress_bar.attr('max', val)
    }

    play() {}

    pause() {}

    finish() {
        this.playing = false
        this.position = 0
        this.current_track = 0
        this.clear_playing_track()
        this.run_event('ended')
    }

    run_event(event) {
        this.dispatchEvent(new Event(event))
    }

    clear_playing_track() {
        this.$playlist.find('.playing').removeClass('playing')
    }

    set_playing_track() {
        $(this.$playlist.children()[this.current_track]).addClass('playing')
    }
}
