import Class from 'classtrophobic'
import $ from 'jquery'
import '../../../common/js/timed_progress.js'
import Player from './player.js'

const mBandcamp = Class({
    extends: Player,
    constructor() {
        this.super('title')
        this.setup_element()
        this.position = 0
        this.playing = false
    },
    setup_element() {
        const $header = $('<div class="header"></div>')

        this.$play_button = $('<button>')
        this.$play_button.on('click', () => {
            if (!this.ready) return
            if (this.playing) this.pause()
            else this.play()
        })

        const $icon = $('<div class="icon">')
        this.$play_button.append($icon)
        $header.append(this.$play_button)

        this.$progress_bar = $(
            document.createElement('progress', { is: 'timed-progress' })
        )
        this.$progress_bar.addClass('progress')
        this.$progress_bar.val(0)
        this.$progress_bar.on('click', e => {
            if (!this.ready) return

            const click_pos = e.pageX - this.$progress_bar.offset().left
            const location = click_pos / this.$progress_bar.width()
            this.position = location * this.duration || 0
            this.play()
        })
        $header.append(this.$progress_bar)

        this.$track_info = $('<div class="trackinfo">')
        $header.append(this.$track_info)

        this.$playlist = $('<div class="playlist"></div>')

        const $style = $(`<!--suppress CssUnresolvedCustomProperty --><style>
            :host {
                display: flex;
                --header: #EEEEEE;
                --playlist: #FFFFFF;
                --playlist-dark: #F6F6F6;
                --text: #333333;
                --highlight: #1DA0C3;
                --play: rgba(0,0,0,0.75);
            }
        
            .player {
                display: flex;
                flex-direction: column;
                width: 100%;
                border: 1px solid var(--playlist-dark);
            }
        
            .header {
                box-sizing: border-box;
                height: 50px;
                display: flex;
            }
            
            .header button {
                box-sizing: border-box;
                display: inline-block;
                height: 100%;
                width: auto;
                border: none;
                padding: 0 10px;
                background: var(--play);
                cursor: default;
            }
            
            .player.ready .header button {
                cursor: pointer;
            }

            .header button .icon {
                box-sizing: border-box;
                width: 30px;
                height: 30px;
                border-style: solid;
                border-width: 15px 0 15px 30px;
                border-color: transparent transparent transparent rgba(255, 255, 255, 0.2);
                transition: 100ms all ease;
            }
            
            .player.ready .header button .icon {
                border-color: transparent transparent transparent #fff;
            }

            .player.playing .header button .icon {
                box-sizing: border-box;
                border-style: double;
                border-width: 0 0 0 30px;
                border-color: #fff;
            }
            
            .header .progress,
            .header .progress::-webkit-progress-bar {
                display: inline-block;
                height: 100%;
                width: 100%;
                color: var(--highlight);
                background-color: var(--header);
                border: 0;
            }
            
            .header .progress::-moz-progress-bar,
            .header .progress::-webkit-progress-value {
                background-color: var(--highlight);
            }
            
            .player.ready .header .progress {
                cursor: col-resize;
            }
            
            .header .trackinfo {
                pointer-events: none;
                position: absolute;
                display: flex;
                align-items: center;
                left: 80px;
                height: 50px;
            }
            
            .playlist {
                overflow: auto;
                height: calc(100% - 50px);
                background: var(--playlist);
            }
            
            .playlist .track {
                color: var(--text);
                padding: 5px 5px;
                font-family: sans-serif;
                font-size: 14px;
            }
            
            .playlist .track .num {
                padding-left: 5px;
            }
            
            .playlist .track .name {
                padding-left: 10px;
            }
            
            .playlist .track:last-of-type {
                border: none;
            }
            
            .playlist .track.playing,
            .playlist .track.playing .num {
                font-weight: bold;
                color: var(--highlight);
            }
            
            .player.ready .playlist .track {
                cursor: pointer;
            }
            
            .player.ready .playlist .track:hover {
                background-color: var(--playlist-dark);
            }
        </style>`)

        this.$wrapper = $('<div class="player"></div>')
        this.$wrapper.append($header)
        this.$wrapper.append(this.$playlist)

        const $shadow = $(this.attachShadow({ mode: 'open' }))
        $shadow.append($style)
        $shadow.append(this.$wrapper)
    },
    connectedCallback() {
        const url = this.getAttribute('url')
        fetch('/bandcamp/data/' + encodeURIComponent(url))
            .then(response => response.json())
            .then(album_data => {
                this.album_data = album_data
                this.tracks = album_data['trackinfo']
                this.ready = true
            })
    },

    get position() {
        return parseInt(this.$progress_bar.val())
    },
    set position(val) {
        const pos = parseInt(val)
        this.$progress_bar.val(pos)
        if (this.audio) this.audio.currentTime = pos / 1000
    },

    get current_track() {
        return this._current_track
    },
    set current_track(val) {
        this._current_track = val
        const track = this.tracks[val]
        this.audio = new Audio(track.file['mp3-128'])
        if (val + 1 >= this.tracks.length)
            this.audio.addEventListener('ended', () => this.finish())
        else this.audio.addEventListener('ended', () => this.play(val + 1))
        this.$track_info.html(
            `<b>${this.album_data['artist']}</b>&nbsp;-&nbsp;${track.title}`
        )
    },

    play(track) {
        if (track != null && track !== this.current_track) {
            if (this.audio) this.audio.pause()
            this.current_track = track
            this.position = 0
        }

        const play_promise = this.audio.play()
        if (play_promise !== undefined) {
            play_promise
                .then(() => {
                    this.duration = this.audio.duration * 1000
                    this.playing = true
                })
                .catch(error => alert('Failed to play....' + error))
        }
    },

    pause() {
        if (this.audio) this.audio.pause()
        this.playing = false
    },
})

customElements.define('m-bandcamp', mBandcamp, { extends: 'div' })
