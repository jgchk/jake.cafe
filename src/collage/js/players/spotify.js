/* eslint-disable no-console */
/* global Spotify */
import Class from 'classtrophobic'
import $ from 'jquery'
import qs from 'querystring'
import uuid from 'uuid/v4.js'
import EventDispatcher from '../../../common/js/event_dispatcher.js'
import m from '../../../common/js/mquery.js'
import Player from './player.js'

const spotify_sdk = new EventDispatcher()
spotify_sdk.ready = false
spotify_sdk.player = null
window.onSpotifyWebPlaybackSDKReady = () => {
    spotify_sdk.ready = true
    spotify_sdk.run('ready')
}

const mSpotify = Class({
    extends: Player,
    constructor() {
        this.super('name')
        this.player_id = uuid()
        this.setup_element()
        this.process_query()
        this.check_authentication()

        if (spotify_sdk.ready) {
            this.setup_callbacks()
            this.ready = true
        } else spotify_sdk.on('ready', () => this.on_sdk_ready())
    },
    setup_element() {
        const $header = $('<div class="header"></div>')

        this.$play_button = $('<button>')
        this.$play_button.on('click', () => {
            if (!this.ready) return
            if (this.playing) this.pause()
            else this.play()
        })

        const $icon = $('<div class="icon">')
        this.$play_button.append($icon)
        $header.append(this.$play_button)

        this.$progress_bar = $(
            document.createElement('progress', { is: 'timed-progress' })
        )
        this.$progress_bar.addClass('progress')
        this.$progress_bar.val(0)
        this.$progress_bar.on('click', e => {
            if (!this.ready) return

            const click_pos = e.pageX - this.$progress_bar.offset().left
            const location = click_pos / this.$progress_bar.width()
            this.position = location * this.duration
            this.play()
        })
        $header.append(this.$progress_bar)

        this.$track_info = $('<div class="trackinfo">')
        $header.append(this.$track_info)

        this.$playlist = $('<div class="playlist"></div>')

        this.$auth_overlay = $('<div>')
        this.$auth_overlay[0].id = 'auth-overlay'

        const $auth_button = $('<button>Login</button>')
        $auth_button.on('click', () => this.authenticate())
        this.$auth_overlay.append($auth_button)

        const $style = $(`<!--suppress CssUnresolvedCustomProperty --><style>
            :host {
                display: flex;
                --header: #1D1A1D;
                --playlist: #050405;
                --text: #FFFFFF;
                --dark-text: #7E7E7E;
                --highlight: #1DB954;
            }
        
            .player {
                display: flex;
                flex-direction: column;
                width: 100%;
            }
        
            .header {
                box-sizing: border-box;
                height: 50px;
                display: flex;
            }
            
            .header button {
                box-sizing: border-box;
                display: inline-block;
                height: 100%;
                width: auto;
                border: none;
                padding: 0 10px;
                background: var(--header);
                cursor: default;
            }
            
            .player.ready .header button {
                cursor: pointer;
            }

            .header button .icon {
                box-sizing: border-box;
                width: 30px;
                height: 30px;
                border-style: solid;
                border-width: 15px 0 15px 30px;
                border-color: transparent transparent transparent rgba(255, 255, 255, 0.2);
                transition: 100ms all ease;
            }
            
            .player.ready .header button .icon {
                border-color: transparent transparent transparent #fff;
            }

            .player.playing .header button .icon {
                box-sizing: border-box;
                border-style: double;
                border-width: 0 0 0 30px;
                border-color: #fff;
            }
            
            .header .progress,
            .header .progress::-webkit-progress-bar {
                display: inline-block;
                height: 100%;
                width: 100%;
                color: var(--highlight);
                background-color: var(--header);
                border: 0;
            }
            
            .header .progress::-moz-progress-bar,
            .header .progress::-webkit-progress-value {
                background-color: var(--highlight);
            }
            
            .player.ready .header .progress {
                cursor: col-resize;
            }
            
            .header .trackinfo {
                pointer-events: none;
                position: absolute;
                display: flex;
                align-items: center;
                left: 80px;
                height: 50px;
                color: var(--text);
            }
            
            .playlist {
                overflow: auto;
                height: calc(100% - 50px);
                background: var(--playlist);
            }
            
            .playlist .track {
                color: var(--text);
                padding: 5px 5px;
                font-family: sans-serif;
                font-size: 14px;
            }
            
            .playlist .track .num {
                padding-left: 5px;
                color: var(--dark-text);
            }
            
            .playlist .track .name {
                padding-left: 10px;
            }
            
            .playlist .track:last-of-type {
                border: none;
            }
            
            .playlist .track.playing,
            .playlist .track.playing .num {
                font-weight: bold;
                color: var(--highlight);
            }
            
            .player.ready .playlist .track {
                cursor: pointer;
            }
            
            .player.ready .playlist .track:hover {
                background-color: rgba(255, 255, 255, 0.2);
            }
            
            #auth-overlay {
                visibility: hidden;
                position: absolute;
                top: 0;
                left: 0;
                z-index: 999;
                width: 100%;
                height: 100%;
                background: rgba(0, 0, 0, 0.5);
                display: flex;
                align-items: center;
                justify-content: center;
            }
            
            #auth-overlay button {
                background: var(--highlight);
                border: none;
                padding: 10px 20px;
                color: var(--text);
                border-radius: 20px;
                cursor: pointer;
            }
        </style>`)

        this.$wrapper = $('<div class="player"></div>')
        this.$wrapper.append($header)
        this.$wrapper.append(this.$playlist)
        this.$wrapper.append(this.$auth_overlay)

        const $shadow = $(this.attachShadow({ mode: 'open' }))
        $shadow.append($style)
        $shadow.append(this.$wrapper)

        const $head = $(document.head)
        const $script = $head.find('#spotify-script')
        if (!$script.length)
            $head.append(
                $(
                    '<script id="spotify-script" src="https://sdk.scdn.co/spotify-player.js"></script>'
                )
            )
    },
    on_sdk_ready() {
        if (spotify_sdk.player == null) {
            spotify_sdk.player = new Spotify.Player({
                name: 'collage',
                getOAuthToken: cb => cb(this.access_token),
            })
            spotify_sdk.player.connect()
        }
        this.setup_callbacks()
    },
    setup_callbacks() {
        spotify_sdk.player.addListener('ready', () => (this.ready = true))
        spotify_sdk.player.addListener('not_ready', () => (this.ready = false))
        spotify_sdk.player.addListener('initialization_error', ({ message }) =>
            console.log(message)
        )
        spotify_sdk.player.addListener('authentication_error', ({ message }) =>
            console.log(message)
        )
        spotify_sdk.player.addListener('account_error', ({ message }) =>
            console.log(message)
        )
        spotify_sdk.player.addListener('playback_error', ({ message }) =>
            console.log(message)
        )
        spotify_sdk.player.addListener('player_state_changed', state => {
            if (!state) return
            if (spotify_sdk.player.current_player === this.player_id) {
                console.log('state', state)

                if (
                    this.playing &&
                    state.paused &&
                    this.current_uri ===
                        this.tracks[this.tracks.length - 1].uri &&
                    state.track_window.current_track.uri === this.tracks[0].uri
                )
                    this.finish()

                this.playing = !state.paused
                this.position = state.position
                this.duration = state.duration
                this.current_track = this.tracks.findIndex(
                    track => track.uri === state.track_window.current_track.uri
                )

                const artist = state.track_window.current_track.artists
                    .map(artist => artist.name)
                    .join(', ')
                this.$track_info.html(
                    `<b>${artist}</b>&nbsp;-&nbsp;${
                        state.track_window.current_track.name
                    }`
                )
            } else this.playing = false
        })
    },
    parse_uri(uri) {
        const parsed = parse_uri(uri)
        if (!parsed) return []

        const id = parsed.split(':').slice(-1)[0]
        if (parsed.includes('album')) {
            return this.auth_fetch(
                `https://api.spotify.com/v1/albums/${id}/tracks`,
                { method: 'GET' }
            ).then(response => response.items)
        } else if (parsed.includes('$playlist')) {
            return this.auth_fetch(
                `https://api.spotify.com/v1/playlists/${id}/tracks`,
                { method: 'GET' }
            ).then(response => response.items.map(item => item.track))
        } else {
            return this.auth_fetch(`https://api.spotify.com/v1/tracks/${id}`, {
                method: 'GET',
            }).then(response => [response])
        }
    },

    play(track) {
        const play = ({
            spotify_uris,
            position,
            current_uri,
            player_instance: {
                _options: { getOAuthToken, id },
            },
        }) => {
            getOAuthToken(() => {
                const body = {}
                body.uris = spotify_uris
                body.position_ms = position
                if (current_uri) body.offset = { uri: current_uri }

                this.auth_fetch(
                    `https://api.spotify.com/v1/me/player/play?device_id=${id}`,
                    { method: 'PUT', body: JSON.stringify(body) }
                )
            })
        }

        if (track != null && track !== this.current_track) {
            this.current_track = track
            this.position = 0
        }

        play({
            player_instance: spotify_sdk.player,
            spotify_uris: this.tracks.map(track => track.uri),
            position: this.position,
            current_uri: this.current_uri,
        })
        spotify_sdk.player.current_player = this.player_id
        this.playing = true
    },
    pause() {
        const pause = ({
            player_instance: {
                _options: { getOAuthToken, id },
            },
        }) => {
            getOAuthToken(() =>
                this.auth_fetch(
                    `https://api.spotify.com/v1/me/player/pause?device_id=${id}`,
                    { method: 'PUT' }
                )
            )
        }

        pause({ player_instance: spotify_sdk.player })
        spotify_sdk.player.current_player = this.player_id
        this.playing = false
    },

    get current_uri() {
        return this.tracks[this.current_track].uri
    },

    // <editor-fold defaultstate="collapsed" desc="Authentication">
    check_authentication() {
        if (this.access_token) {
            this.is_access_token_expired().then(expired => {
                if (expired) {
                    if (this.refresh_token)
                        this.refresh_auth().then(
                            () => (this.authenticated = true)
                        )
                    else this.authenticated = false
                } else {
                    this.authenticated = true
                }
            })
        } else {
            if (this.refresh_token)
                this.refresh_auth().then(() => (this.authenticated = true))
            else this.authenticated = false
        }
    },
    auth_fetch(input, init) {
        const data = () => {
            return Object.assign(
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${this.access_token}`,
                    },
                },
                init
            )
        }
        return fetch(input, data())
            .then(response => (response.status === 204 ? {} : response.json()))
            .then(json => {
                if (
                    json.error &&
                    json.error.message === 'The access token expired'
                ) {
                    return this.refresh_auth().then(() =>
                        fetch(input, data()).then(response => response.json())
                    )
                } else return json
            })
    },
    get refresh_token() {
        return localStorage.getItem('spotify_refresh_token')
    },
    set refresh_token(val) {
        localStorage.setItem('spotify_refresh_token', val)
    },
    get access_token() {
        return sessionStorage.getItem('spotify_access_token')
    },
    set access_token(val) {
        sessionStorage.setItem('spotify_access_token', val)
    },
    get authenticated() {
        return this.$wrapper.hasClass('authenticated')
    },
    set authenticated(val) {
        if (val) {
            this.$auth_overlay.css('visibility', 'hidden')
            this.parse_uri(this.getAttribute('uri')).then(
                tracks => (this.tracks = tracks)
            )
        } else this.$auth_overlay.css('visibility', 'visible')
    },
    authenticate() {
        window.location =
            '/spotify/login?' + qs.stringify({ from: window.location.pathname })
    },
    is_access_token_expired() {
        return fetch('https://api.spotify.com/v1/me', {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.access_token}`,
            },
        })
            .then(response => response.json())
            .then(
                json =>
                    json.error &&
                    json.error.message === 'The access token expired'
            )
    },
    refresh_auth() {
        return fetch(
            '/spotify/refresh_token?' +
                qs.stringify({
                    refresh_token: this.refresh_token,
                })
        )
            .then(response => response.json())
            .then(json => {
                this.access_token = json.access_token
                return this.access_token
            })
    },
    process_query() {
        m.with(m.query('access_token'), token => (this.access_token = token))
        m.with(m.query('refresh_token'), token => (this.refresh_token = token))

        if (m.query('access_token') || m.query('refresh_token'))
            window.history.pushState(
                {},
                'collage',
                window.location.href.split('?')[0]
            )
    },
    // </editor-fold>
})

customElements.define('m-spotify', mSpotify, { extends: 'div' })

export function parse_uri(uri) {
    uri = uri.split('?')[0]
    if (
        uri.match(/^spotify:(?:track|album|artist|playlist):[a-zA-Z0-9]{22}$/)
    ) {
        return uri
    } else if (uri.match(/^spotify:user:[^:]+:playlist:[a-zA-Z0-9]{22}$/)) {
        return uri
    } else if (
        uri.match(
            /^https?:\/\/(?:open|play)\.spotify\.com\/(?:track|album|artist|playlist)\/[a-zA-Z0-9]{22}\/?$/
        )
    ) {
        return (
            'spotify:' +
            uri
                .replace(/^https?:\/\/(?:open|play)\.spotify\.com\//, '')
                .split('/')
                .join(':')
        )
    } else if (
        uri.match(
            /^https?:\/\/(?:open|play)\.spotify\.com\/user\/[^:]+\/playlist\/[a-zA-Z0-9]{22}\/?$/
        )
    ) {
        return (
            'spotify:' +
            uri
                .replace(/^https?:\/\/(?:open|play)\.spotify\.com\//, '')
                .split('/')
                .join(':')
        )
    }
    return false
}
