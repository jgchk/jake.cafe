/* global YT */
import Class from 'classtrophobic'
import $ from 'jquery'
import EventDispatcher from '../../../common/js/event_dispatcher.js'

const youtube_sdk = new EventDispatcher()
youtube_sdk.ready = false
window.onYouTubeIframeAPIReady = () => {
    youtube_sdk.ready = true
    youtube_sdk.run('ready')
}

const mYoutube = Class({
    extends: HTMLDivElement,
    constructor() {
        this.super()
        this.ready = false
        this.setup_element()

        if (youtube_sdk.ready) this.on_sdk_ready()
        else youtube_sdk.on('ready', () => this.on_sdk_ready())
    },

    setup_element() {
        const $head = $(document.head)
        const $script = $head.find('#youtube-script')
        if (!$script.length)
            $head.append(
                $(
                    '<script id="youtube-script" src="https://www.youtube.com/player_api"></script>'
                )
            )

        this.$wrapper = $(
            '<iframe width="100%" height="100%" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>'
        )
        const $shadow = $(this.attachShadow({ mode: 'open' }))
        $shadow.append(this.$wrapper)
    },

    connectedCallback() {
        this.url = this.getAttribute('url')
        this.$wrapper.attr(
            'src',
            `https://www.youtube-nocookie.com/embed/${
                this.youtube_id
            }?enablejsapi=1`
        )
    },

    on_sdk_ready() {
        this.player = new YT.Player(this.$wrapper[0])
        this.setup_callbacks()
    },

    setup_callbacks() {
        this.player.addEventListener('onReady', () => {
            this.ready = true
            this.run_event('ready')
        })
        this.player.addEventListener('onStateChange', e => {
            const state = e.data
            if (state === 0) this.run_event('ended')
            else if (state === 1) this.run_event('play')
            else if (state === 2) this.run_event('pause')
        })
    },

    play() {
        this.player.playVideo()
    },

    pause() {
        this.player.pauseVideo()
    },

    run_event(event) {
        this.dispatchEvent(new Event(event))
    },

    get youtube_id() {
        const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/
        const match = this.url.match(regExp)

        if (match && match[2].length === 11) {
            return match[2]
        } else {
            return 'error'
        }
    },
})

customElements.define('m-youtube', mYoutube, { extends: 'div' })
