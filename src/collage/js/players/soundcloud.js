/* global SC */
import Class from 'classtrophobic'
import $ from 'jquery'

const mSoundcloud = Class({
    extends: HTMLDivElement,
    constructor() {
        this.super()
        this.ready = false
        this.setup_element()
    },

    setup_element() {
        $.ajaxSetup({ cache: true })
        $.getScript('https://w.soundcloud.com/player/api.js', () => {
            const $wrapper = $(
                '<iframe width="100%" height="100%" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=">'
            )
            this.player = SC.Widget($wrapper[0])
            this.setup_callbacks()
            this.player.load(this.url, {
                auto_play: false,
                buying: false,
                sharing: false,
                download: false,
                show_artwork: false,
                show_playcount: false,
                show_user: false,
                single_active: true,
                hide_related: true,
                show_comments: false,
                show_reposts: false,
                show_teaser: false,
                visual: true,
            })

            const $shadow = $(this.attachShadow({ mode: 'open' }))
            $shadow.append($wrapper)
        })
    },

    connectedCallback() {
        this.url = this.getAttribute('url')
    },

    setup_callbacks() {
        this.player.bind(SC.Widget.Events.PLAY, () => this.run_event('play'))
        this.player.bind(SC.Widget.Events.PAUSE, () => this.run_event('pause'))
        this.player.bind(SC.Widget.Events.FINISH, () =>
            this.run_event('ended')
        )
        this.player.bind(SC.Widget.Events.READY, () => {
            this.ready = true
            this.run_event('ready')
        })
    },

    play() {
        this.player.play()
    },

    pause() {
        this.player.pause()
    },

    run_event(event) {
        this.dispatchEvent(new Event(event))
    },
})

customElements.define('m-soundcloud', mSoundcloud, { extends: 'div' })
