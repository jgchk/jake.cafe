import $ from 'jquery'

class Dropbox {
    constructor(
        box_id,
        input_id,
        progress_id,
        on_file_uploaded,
        on_text_dropped
    ) {
        this.on_file_uploaded = on_file_uploaded
        this.on_text_dropped = on_text_dropped

        this.$dropbox = $(`#${box_id}`)
        this.$dropbox.on('dragenter dragover dragleave drop', e =>
            e.preventDefault()
        )
        this.$dropbox.on('dragleave drop', () => this.hide())
        this.$dropbox.on('drop', e => this.handle_drop(e))

        const $input = $(`#${input_id}`)
        $input.on('change', () => {
            if ($input[0] instanceof HTMLInputElement)
                this.handle_files($input[0].files)
        })

        this.$progress_bar = $(`#${progress_id}`)
        this.upload_progress = []

        window.addEventListener('dragenter', () => this.show())
        window.addEventListener('paste', e => this.handle_paste(e))
    }

    show() {
        this.$dropbox.css('visibility', 'visible')
    }

    hide() {
        this.$dropbox.css('visibility', 'hidden')
    }

    handle_drop(e) {
        this.handle_data_transfer(e.originalEvent.dataTransfer)
    }

    handle_paste(e) {
        this.handle_data_transfer(e.clipboardData)
    }

    handle_data_transfer(dt) {
        if (dt == null) return
        const items = dt.items
        for (let item of items) {
            if (item.kind === 'string' && item.type === 'text/plain') {
                item.getAsString(s => this.handle_text(s))
            } else if (item.kind === 'file') {
                const file = item.getAsFile()
                if (file) this.handle_files([file])
            }
        }
    }

    handle_files(files) {
        files = [...files]
        if (this.$progress_bar.length) this.initialize_progress(files.length)
        files.forEach((file, i) => this.upload_file(file, i))
        files.forEach(file => this.preview_file(file))
    }

    upload_file(file, i) {
        const url = '/collage/upload'
        const xhr = new XMLHttpRequest()
        xhr.open('POST', url, true)

        xhr.upload.addEventListener('progress', e => {
            if (this.$progress_bar.length)
                this.update_progress(i, (e.loaded * 100.0) / e.total || 0)
        })
        xhr.addEventListener('readystatechange', () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                this.on_file_uploaded(JSON.parse(xhr.response))
            } else if (xhr.readyState === 4 && xhr.status !== 200) {
                alert('Error uploading file(s)...')
            }
        })

        const form_data = new FormData()
        form_data.append('file', file)
        xhr.send(form_data)
    }

    preview_file(file) {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onloadend = () => {
            const $img = $(`<img src="${reader.result}">`)
            const $gallery = $('#gallery')
            if ($gallery.length) $gallery.append($img)
        }
    }

    initialize_progress(num_files) {
        this.$progress_bar.value(0)
        this.upload_progress = []
        for (let i = num_files; i > 0; i--) this.upload_progress.push(0)
    }

    update_progress(file_number, percent) {
        this.upload_progress[file_number] = percent
        this.$progress_bar.value(
            this.upload_progress.reduce((tot, cur) => tot + cur, 0) /
                this.upload_progress.length
        )
    }

    handle_text(text) {
        this.on_text_dropped(text)
    }
}

export default Dropbox
