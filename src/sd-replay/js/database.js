import Dexie from 'dexie'

export default class Database {
    constructor() {
        this.db = new Dexie('Showdown')
        this.db.version(1).stores({
            battles: '&id',
        })
    }

    store_battle(id) {
        return this.db.battles.get(id).then(battle => {
            if (battle === undefined)
                return fetch(`/showdown/replay/${id}`)
                    .then(response => response.text())
                    .then(battle_log => {
                        battle = { id: id, log: battle_log }
                        return this.db.battles
                            .add(battle)
                            .then(() => Promise.resolve(battle))
                    })
            else return Promise.resolve(false)
        })
    }

    all_battles() {
        return this.db.battles.toArray()
    }
}
