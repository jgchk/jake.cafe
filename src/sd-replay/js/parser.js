import Battle from './battle.js'

export default class Parser {
    constructor(log) {
        this.lines = log.split('\n')
        this.index = 0
        this.history = []
        this.battle = new Battle()
    }

    back() {
        if (!this.backable()) return
        this.history.pop()
        const [index, line, result] = this.history[this.history.length - 1]
        this.index = index + 1
        return line
    }

    backable = () => this.history.length > 1

    next() {
        const line = this.lines[this.index]
        const result = this.parse(line)
        this.history.push([this.index, line, result])
        this.index++
        return line
    }

    nextable = () => this.index < this.lines.length

    parse(line) {
        if (line.startsWith('|')) line = line.substring(1)
        else return // if the line doesn't start with |, it's just a message

        const line_data = line.split('|')
        if (line_data.length === 1 && line_data[0] === '') return // empty line

        const command = line_data[0]
        const args = line_data.slice(1)

        const handler = this.handlers[command]
        if (handler) handler(...args)
        else console.log('ERROR: no handler for ' + command)

        return JSON.stringify(this.battle)
    }

    handlers = {
        player: (player, username, avatar) =>
            this.battle.add_player(player, username, avatar),
        switch: (pokemon, details, hp_status) => {
            const [player, name] = parse_pokemon(pokemon)
            const [species, level, gender, shiny] = parse_details(details)
            const hp_pct = parse_hp_status(hp_status)
            this.battle.add_pokemon(
                player,
                name,
                species,
                shiny,
                gender,
                level,
                hp_pct
            )
        },
        move: (pokemon, move, target) => {
            const [player, name] = parse_pokemon(pokemon)
            this.battle.add_move(player, name, move)
        },
    }
}

function parse_pokemon(pokemon) {
    const pokemon_data = pokemon.split(': ')
    const player = pokemon_data[0].substring(0, 2)
    const name = pokemon_data[1]
    return [player, name]
}

function parse_details(details) {
    const details_data = details.split(', ').reverse()
    const species = details_data.pop()
    const level =
        details_data.length &&
        details_data[details_data.length - 1].startsWith('L')
            ? parseInt(details_data.pop().substring(1))
            : 100
    const gender =
        details_data.length && details_data[details_data.length - 1] !== 'shiny'
            ? details_data.pop()
            : 'N'
    const shiny = details_data.length ? details_data.pop() === 'shiny' : false

    return [species, level, gender, shiny]
}

function parse_hp_status(hp_status) {
    const hp_data = hp_status.split('/')
    const hp_pct = parseInt(hp_data[0]) / parseInt(hp_data[1])
    return hp_pct
}
