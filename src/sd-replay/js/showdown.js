import '../css/showdown.css'
import $ from 'jquery'
import Parser from './parser.js'
import Database from './database.js'

const db = new Database()
db.all_battles().then(battles => {
    for (const battle of battles) add_battle(battle)
})

const refresh_btn = $('#refresh')
refresh_btn.on('click', () => {
    fetch('/showdown/replays/gen7randombattle')
        .then(response => response.json())
        .then(battle_ids => {
            for (const battle_id of battle_ids.reverse())
                db.store_battle(battle_id).then(battle => {
                    if (battle) add_battle(battle)
                })
        })
})

const battle_list = $('#battle-list')
const data_win = $('#data')
function add_battle(battle) {
    const battle_btn = $(
        `<div class="battle" id="${battle.id}">${battle.id}</div>`
    )
    battle_btn.on('click', () => {
        $('.battle.selected').removeClass('selected')
        battle_btn.addClass('selected')
        select_battle(battle)
    })
    battle_list.prepend(battle_btn)
}

let parser
function select_battle(battle) {
    data_win.empty()
    for (const line of battle.log.split('\n'))
        data_win.append($(`<div class="line">${line}</div>`))

    parser = new Parser(battle.log)
    update_buttons()
    line_display.text('')
    battle_display.text('')
}

const battle_display = $('#state')
const back_btn = $('#back')
const next_btn = $('#next')
const line_display = $('#line')

next_btn.prop('disabled', true)
next_btn.on('click', () => {
    const line = parser.next()
    line_display.text(line)
    update_buttons()
    update_data_line()
    update_battle_display()
})

back_btn.prop('disabled', true)
back_btn.on('click', () => {
    const line = parser.back()
    line_display.text(line)
    update_buttons()
    update_data_line()
    update_battle_display()
})

function update_buttons() {
    back_btn.prop('disabled', !parser.backable())
    next_btn.prop('disabled', !parser.nextable())
}

function update_data_line() {
    $('.line.selected').removeClass('selected')
    data_win
        .children()
        .eq(parser.index - 1)
        .addClass('selected')
}

function update_battle_display() {
    battle_display.text(JSON.stringify(parser.battle, null, 2))
}
