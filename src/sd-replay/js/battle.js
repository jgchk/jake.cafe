import dex from './pokedex.js'

export default class Battle {
    constructor() {
        this.players = {}
    }

    add_player(player, username, avatar) {
        this.players[player] = {
            username: username,
            avatar: avatar,
            pokemon: {},
        }
    }

    add_pokemon(player, name, species, shiny, gender, level, hp_pct) {
        this.players[player].pokemon[name] = {
            species: species,
            shiny: shiny,
            gender: gender,
            level: level,
            hp_pct: hp_pct,
            stats: calculate_stats(species, level),
            moves: [],
        }
    }

    add_move(player, name, move) {
        const mon = this.players[player].pokemon[name]
        if (!mon.moves.includes(move)) mon.moves.push(move)
    }
}

function calculate_stats(species, level) {
    const id = species.toLowerCase().replace('-', '')
    const mon_data = dex[id]
    const base_stats = mon_data.baseStats
    const stats = {}
    for (const stat of Object.keys(base_stats)) {
        const min = get_stat(base_stats[stat], stat === 'hp', level, 31, 0, 1.0)
        const max = get_stat(
            base_stats[stat],
            stat === 'hp',
            level,
            31,
            252,
            1.0
        )

        const min_minus =
            stat === 'hp'
                ? min
                : get_stat(base_stats[stat], false, level, 0, 0, 0.9)
        const max_plus =
            stat === 'hp'
                ? max
                : get_stat(base_stats[stat], false, level, 31, 252, 1.1)
        stats[stat] = [min_minus, min, max, max_plus]
    }
    return stats
}

function get_stat(base_stat, is_hp, level, iv, ev, nature_multiplier) {
    if (is_hp) {
        if (base_stat === 1) return 1
        return Math.floor(
            (Math.floor(
                2 * base_stat + (iv || 0) + Math.floor((ev || 0) / 4) + 100
            ) *
                level) /
                100 +
                10
        )
    }
    let val = Math.floor(
        (Math.floor(2 * base_stat + (iv || 0) + Math.floor((ev || 0) / 4)) *
            level) /
            100 +
            5
    )
    if (nature_multiplier && !is_hp) val *= nature_multiplier
    return Math.floor(val)
}
