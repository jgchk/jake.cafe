class tracker {
    constructor(...events) {
        this.x = 0
        this.y = 0
        this.cb = () => {}
        events.forEach(event =>
            document.addEventListener(event, e => this.update(e))
        )
    }

    callback(cb) {
        this.cb = cb
        return this
    }

    update(e) {
        this.x = e.clientX + document.scrollingElement.scrollLeft
        this.y = e.clientY + document.scrollingElement.scrollTop
        this.cb(this.x, this.y)
    }
}

export default tracker
