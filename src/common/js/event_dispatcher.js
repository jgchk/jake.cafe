export default class EventDispatcher {
    constructor() {
        this.listeners = {}
    }

    on(events, listener) {
        for (const event of events.split(' ')) {
            if (!this.listeners[event]) this.listeners[event] = []
            this.listeners[event].push(listener)
        }
    }

    off(events, listener) {
        for (const event of events.split(' ')) {
            this.listeners[event] = this.listeners[event].filter(
                c => c !== listener
            )
        }
    }

    run(event, ...args) {
        const listeners = this.listeners[event] || []
        for (const listener of listeners) listener(...args)
    }
}
