const m = {
    with: (variable, func) => {
        if (variable != null) return func(variable)
    },
    query: name => {
        const match = RegExp('[?&]' + name + '=([^&]*)').exec(
            window.location.search
        )
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '))
    },
    path: () => {
        return window.location.href.split(/[?#]/)[0]
    },
}

export default m
