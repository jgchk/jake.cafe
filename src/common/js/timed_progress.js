import Class from 'classtrophobic'

const TimedProgress = Class({
    extends: HTMLProgressElement,
    constructor() {
        this.super()
        this.interval = 250
    },
    static: {
        get observedAttributes() {
            return ['playing']
        },
    },
    get playing() {
        return this.getAttribute('playing') === 'true'
    },
    set playing(val) {
        if (val) {
            this.setAttribute('playing', true)
        } else {
            this.setAttribute('playing', false)
        }
    },
    get value() {
        return parseInt(this.getAttribute('value')) || 0
    },
    set value(val) {
        this.setAttribute('value', val)
    },
    get max() {
        return parseInt(this.getAttribute('max')) || 0
    },
    set max(val) {
        this.setAttribute('max', val)
    },
    attributeChangedCallback() {
        if (this.playing) {
            this.start()
        } else {
            this.stop()
        }
    },

    start() {
        if (!this.playing) this.playing = true
        if (this.timer) clearInterval(this.timer)
        this.timer = setInterval(() => this.update_progress(), this.interval)
    },
    stop() {
        if (this.playing) this.playing = false
        clearInterval(this.timer)
    },

    update_progress() {
        this.value = this.value + this.interval
    },
})

customElements.define('timed-progress', TimedProgress, { extends: 'progress' })
