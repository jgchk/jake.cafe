const path = require('path')
const webpack = require('webpack')
const HtmlWebPackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: {
        main: [
            'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
        ],
        index: ['./src/index/js/index.js'],
        collage: ['./src/collage/js/collage.js'],
        neat: ['./src/neat/js/neat.js'],
        showdown: ['./src/showdown/js/showdown.js'],
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
        filename: '[name].js',
    },
    mode: 'development',
    target: 'web',
    devtool: '#source-map',
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                options: {
                    emitWarning: true,
                    failOnError: false,
                    failOnWarning: false,
                },
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                // Loads the javacript into views template provided.
                // Entry point is set below in HtmlWebPackPlugin in Plugins
                test: /\.html$/,
                use: [
                    {
                        loader: 'views-loader',
                        //options: { minimize: true }
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader',
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader'],
            },
            {
                test: /\.pug$/,
                use: ['pug-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebPackPlugin({
            title: 'jake.cafe',
            chunks: ['main', 'index'],
            template: './src/index/views/index.pug',
            filename: './index.html',
        }),
        new HtmlWebPackPlugin({
            title: 'collage',
            chunks: ['main', 'collage'],
            template: './src/collage/views/collage.pug',
            filename: './collage.html',
        }),
        new HtmlWebPackPlugin({
            title: 'neat',
            chunks: ['main', 'neat'],
            template: './src/neat/views/neat.pug',
            filename: './neat.html',
        }),
        new HtmlWebPackPlugin({
            title: 'showdown',
            chunks: ['main', 'showdown'],
            template: './src/showdown/views/showdown.pug',
            filename: './showdown.html',
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
    ],
}
