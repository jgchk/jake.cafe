const path = require('path')
const HtmlWebPackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = {
    entry: {
        index: ['./src/index/js/index.js'],
        collage: ['./src/collage/js/collage.js'],
        showdown: ['./src/showdown/js/showdown.js'],
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
        filename: '[name].js',
    },
    target: 'web',
    devtool: '#source-map',
    // Webpack 4 does not have a CSS minifier, although
    // Webpack 5 will likely come with one
    optimization: {
        minimizer: [
            new TerserPlugin({
                cache: true,
                parallel: true,
                sourceMap: true, // set to true if you want JS source maps
            }),
            new OptimizeCSSAssetsPlugin({}),
        ],
    },
    module: {
        rules: [
            {
                // Transpiles ES6-8 into ES5
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                // Loads the javacript into views template provided.
                // Entry point is set below in HtmlWebPackPlugin in Plugins
                test: /\.html$/,
                use: [
                    {
                        loader: 'views-loader',
                        options: { minimize: true },
                    },
                ],
            },
            {
                // Loads images into CSS and Javascript files
                test: /\.(png|svg|jpg|gif)$/,
                use: [{ loader: 'url-loader' }],
            },
            {
                // Loads CSS into a file when you import it via Javascript
                // Rules are set in MiniCssExtractPlugin
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader',
                ],
            },
            {
                test: /\.pug$/,
                use: ['pug-loader'],
            },
        ],
    },
    plugins: [
        new HtmlWebPackPlugin({
            title: 'jake.cafe',
            chunks: ['index'],
            template: './src/index/views/index.pug',
            filename: './index.html',
        }),
        new HtmlWebPackPlugin({
            title: 'collage',
            chunks: ['collage'],
            template: './src/collage/views/collage.pug',
            filename: './collage.html',
        }),
        new HtmlWebPackPlugin({
            title: 'showdown',
            chunks: ['main', 'showdown'],
            template: './src/showdown/views/showdown.pug',
            filename: './showdown.html',
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
    ],
}
