module.exports = {
    extends: ['eslint:recommended'],
    parser: 'babel-eslint',
    env: {
        browser: true,
        node: true,
        jest: true,
        es6: true,
    },
}
