#!/usr/bin/env bash
ssh jake@jake.cafe "cd jake.cafe && git pull && yarn install && yarn buildProd && pm2 restart all"